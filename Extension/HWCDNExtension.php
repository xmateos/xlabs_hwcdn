<?php

namespace XLabs\HWCDNBundle\Extension;

use XLabs\HWCDNBundle\Services\HWCDN;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class HWCDNExtension extends AbstractExtension
{
    private $xlabs_hwcdn;
    
    public function __construct(HWCDN $xlabs_hwcdn)
    {
        $this->xlabs_hwcdn = $xlabs_hwcdn;
    }
    
    public function getFunctions()
    {
        return array(
            new TwigFunction('getCDNResource', array($this, 'getCDNResource')),
        );
    }
    
    public function getFilters()
    {
        return array();
    }

    public function getCDNResource($aOptions)
    {
        /* Local dev assets load */
        /*if(strlen(strstr($aOptions['media_path'], 'assets/images')) > 0)
        {
            return $aOptions['media_path'];
        }*/
        /* END - Local dev assets load */
        return $this->xlabs_hwcdn->getCDNResource($aOptions);
    }
}