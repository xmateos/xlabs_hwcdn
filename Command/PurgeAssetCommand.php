<?php

namespace XLabs\HWCDNBundle\Command;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

class PurgeAssetCommand extends ContainerAwareCommand
{

    protected function configure()
    {
        $this->setName('purge:asset')
            ->addArgument('asset_url',InputOption::VALUE_REQUIRED);
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $asset = $input->getArgument('asset_url');

        $xlabs_hwcdn = $this->getContainer()->get('xlabs_hwcdn');
        $purge_success = $xlabs_hwcdn->purge(array(
            $asset
        ));

        dump($purge_success);
    }
}