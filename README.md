A redis driven like engine.

## Installation ##

Install through composer:

```bash
php -d memory_limit=-1 composer.phar require xlabs/hwcdnbundle
```

In your AppKernel

```php
public function registerbundles()
{
    return [
    	...
    	...
    	new XLabs\HWCDNBundle\XLabsHWCDNBundle(),
    ];
}
```

## Configuration sample
Default values are shown below:
``` yml
# app/config/config.yml

x_labs_hwcdn:
    public_url: 'https://public.domain.ext'
    tokenized_url: 'https://signed.domain.ext'
    account: ############
    secretKey: ############
    expiration_ttl: 7200
    api_username: ############
    api_password: ############
```

### Usage ###
Instantiate the service from a Controller by:
```php
$this->get('xlabs_hwcdn')->...
```
### Usage (TWIG) ###
Instantiate the service from a Controller by:
```php
{{ getCDNResource({
    'media_path' => '...',
    'tokenize' => false, // protect resource with token
    'expiration_ttl' => 7200,
    'force_download' => false // create a CDN download link
}) }}
```