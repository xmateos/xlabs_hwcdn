<?php

namespace XLabs\HWCDNBundle\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * This is the class that validates and merges configuration from your app/config files.
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/configuration.html}
 */
class Configuration implements ConfigurationInterface
{
    /**
     * {@inheritdoc}
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('x_labs_hwcdn');

        // Here you should define the parameters that are allowed to
        // configure your bundle. See the documentation linked above for
        // more information on that topic.
        $rootNode
            ->children()
                //->scalarNode('domain')->defaultValue('stiffia.com')->end()
                ->scalarNode('public_url')->defaultValue('public.stiffia.com')->end()
                ->scalarNode('tokenized_url')->defaultValue('signed,stiffia.com')->end()
                ->scalarNode('account')->end()
                ->scalarNode('secretKey')->end()
                ->integerNode('expiration_ttl')->end()
                ->scalarNode('api_username')->end()
                ->scalarNode('api_password')->end()
            ->end()
        ;

        return $treeBuilder;
    }
}
