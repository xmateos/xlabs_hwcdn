<?php

namespace XLabs\HWCDNBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;

/*
 * NatNet HighWinds CDN
 */
class HWCDN
{
    private $request;
    private $config;
    private $kernel_root_dir;

    public function __construct(RequestStack $request_stack, $config, $kernel_root_dir)
    {
        $this->request = $request_stack->getCurrentRequest();
        $this->config = $config;
        $this->kernel_root_dir = $kernel_root_dir;
    }

    public function getCDNResource($aOptions)
    {
        $default_options = array(
            'media_path' => false,
            'tokenize' => false,
            'ip_protection' => false,
            'expiration_ttl' => false,
            'force_download' => false
        );
        $aOptions = array_merge($default_options, $aOptions);

        // if is local dev, check if the resource exists locally before checking the live cdn
        if($this->request)
        {
            $customLocalDevelopmentHeader = $this->request->headers->get('x-stiffia-local-dev');
            if($customLocalDevelopmentHeader)
            {
                $web_folder = realpath($this->kernel_root_dir.'/../web/');
                $toCheck = realpath($web_folder.$aOptions['media_path']);
                if(is_file($toCheck))
                {
                    return $aOptions['media_path'];
                }
            }
        }

        //$cdnZone = $aOptions['tokenize'] ? ($aOptions['force_download'] ? 'cdn-file' : 'cdn') : 'cdn.static';
        //$baseURL = 'https://'.$cdnZone.'.'.$this->config['domain'];
        $baseURL = $aOptions['tokenize'] ? $this->config['tokenized_url'] : $this->config['public_url'];
        $expire = $aOptions['expiration_ttl'] ? (time() + $aOptions['expiration_ttl']) : (time() + $this->config['expiration_ttl']);

        //$expire = strtotime('-6 hours', time()) + 3600;
        $url = $baseURL.$aOptions['media_path'];
        $url .= $aOptions['force_download'] ? '?attachment=attachment' : '';
        if($aOptions['tokenize'])
        {
            $signing_url = $aOptions['media_path'].'?';
            $signing_url .= $aOptions['force_download'] ? 'attachment=attachment&' : '';
            /*if($aOptions['ip_protection'])
            {
                $ip = trim($_SERVER['REMOTE_ADDR']);
                if($ip == '127.0.0.1')
                {
                    $externalContent = file_get_contents('http://checkip.dyndns.com/');
                    preg_match('/Current IP Address: \[?([:.0-9a-fA-F]+)\]?/', $externalContent, $m);
                    $ip = trim($m[1]);
                }
                $signing_url .= 'ip='.ip2long($ip).'&';
            }
            //$signing_url .= $aOptions['ip_protection'] ? 'ip='.ip2long(trim($_SERVER['REMOTE_ADDR'])).'&' : '';
            //$signing_url .= 's='.$this->session->getId().'&';*/
            $signing_url .= 'expire='.$expire.'&secret='.$this->config['secretKey'];
            //dump($_SERVER, $ip, $signing_url);
            $token = MD5($signing_url);
            $url .= '?expire='.$expire.($aOptions['ip_protection'] ? '&ip='.ip2long(trim($_SERVER['REMOTE_ADDR'])) : '').'&token='.$token;
        }
        return $url;
    }

    public function purge($arrFilePatterns)
    {
        $token = $this->getAuthenticationToken();

        $purgeURL = "https://striketracker.highwinds.com/api/accounts/".$this->config['account']."/purge";
        //Create send data
        $request_params = array(
            'list' => array()
        );
        foreach($arrFilePatterns as $filePattern)
        {
            $request_params['list'][] = array(
                'url' => $filePattern,
                'recursive' => true
            );
        }
        $data = json_encode($request_params);

        //Send the request to HW
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $purgeURL);
        curl_setopt($ch, CURLOPT_PORT , 443);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
                'Authorization: Bearer '.$token,
                'Content-Type: application/json',
                'Accept: application/json',
                'Content-length: '.strlen($data))
        );
        $head = curl_exec($ch);
        $httpCode = curl_getinfo($ch);
        curl_close($ch);
        return $httpCode && $httpCode['http_code'] == 200;
    }

    public function getAuthenticationToken()
    {
        // Authentication first
        $authURL = 'https://striketracker.highwinds.com/auth/token';
        $authData = 'username='.$this->config['api_username'].'&password='.$this->config['api_password'].'&grant_type=password';
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $authURL);
        curl_setopt($ch, CURLOPT_PORT , 443);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FORBID_REUSE, 1);
        curl_setopt($ch, CURLOPT_FRESH_CONNECT, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $authData);

        $response = curl_exec($ch);
        curl_close($ch);
        $token = json_decode($response, true);
        return $token['access_token'];
    }
}